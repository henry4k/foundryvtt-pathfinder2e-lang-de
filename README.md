# German language support for Pathfinder 2e in Foundry VTT

## Installation

1. Copy this link and use it in Foundrys module manager to install the module
    > https://gitlab.com/henry4k/foundryvtt-pathfinder2e-lang-de/-/raw/master/module.json
2. Install the module Babele in Foundrys module manager
3. Enable Babele and this module in your worlds module settings

Note: You need to keep the field "Translation file path" of the Babele settings empty! Otherwise our translations will not work.
